<?php

namespace Savannabits\Mongodb\Concerns;

use Closure;
use Exception;
use Throwable;

trait ManagesTransactions
{
    use \Illuminate\Database\Concerns\ManagesTransactions;
    /**
     * Execute a Closure within a transaction.
     *
     * @param  \Closure  $callback
     * @param  int  $attempts
     * @return mixed
     *
     * @throws \Exception|\Throwable
     */
    public function transaction(Closure $callback, $attempts = 1)
    {
        //First setup a new session
        /**
         * @var \MongoDB\Driver\Session $session
         */
        $session = $this->getMongoClient()->startSession();
        for ($currentAttempt = 1; $currentAttempt <= $attempts; $currentAttempt++) {
            $this->beginMongoTransaction($session);
            // We'll simply execute the given callback within a try / catch block and if we
            // catch any exception we can rollback this transaction so that none of this
            // gets actually persisted to a database or stored in a permanent fashion.
            try {
                return tap($callback($this), function ($result) use($session) {
                    $this->commitMongo($session);
                    $session->endSession();
                });
            }

            // If we catch an exception we'll rollback this transaction and try again if we
            // are not out of attempts. If we are out of attempts we will just throw the
            // exception back out and let the developer handle an uncaught exceptions.
            catch (Exception $e) {
                $this->handleMongoTransactionException(
                     $e,$session, $currentAttempt, $attempts
                );
            } catch (Throwable $e) {
                $this->rollBackMongo($session);
                $session->endSession();
                throw $e;
            }
        }
    }

    /**
     * Handle an exception encountered when running a transacted statement.
     *
     * @param  \Exception $e
     * @param \MongoDB\Driver\Session $session
     * @param  int $currentAttempt
     * @param  int $maxAttempts
     * @return void
     *
     * @throws Exception
     */
    protected function handleMongoTransactionException($e, \MongoDB\Driver\Session $session, $currentAttempt, $maxAttempts)
    {
        // On a deadlock, MySQL rolls back the entire transaction so we can't just
        // retry the query. We have to throw this exception all the way out and
        // let the developer handle it in another way. We will decrement too.
        if ($this->causedByDeadlock($e) &&
            $this->transactions > 1) {
            $this->transactions--;

            throw $e;
        }

        // If there was an exception we will rollback this transaction and then we
        // can check if we have exceeded the maximum attempt count for this and
        // if we haven't we will return and try this query again in our loop.
        $this->rollBackMongo($session);

        if ($this->causedByDeadlock($e) &&
            $currentAttempt < $maxAttempts) {
            return;
        }

        throw $e;
    }

    /**
     * Start a new database transaction.
     * @param  \MongoDb\Driver\Session $session
     * @return void
     * @throws \Exception
     * @throws Throwable
     */
    public function beginMongoTransaction(\MongoDB\Driver\Session $session)
    {
        $this->createMongoTransaction($session);

        $this->transactions++;

        $this->fireConnectionEvent('beganTransaction');
    }

    /**
     * Create a transaction within the database.
     * @param \MongoDb\Driver\Session $session
     * @return void
     * @throws Throwable
     */
    protected function createMongoTransaction(\MongoDB\Driver\Session $session)
    {
        if ($this->transactions == 0) {
            try {
                $session->startTransaction([
                    'readConcern' => new \MongoDB\Driver\ReadConcern("snapshot"),
                    'writeConcern' => new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY)
                ]);
            } catch (Exception $e) {
                $this->handleBeginMongoTransactionException($e, $session);
            }
        }
    }

    /**
     * Create a save point within the database.
     *
     * @return void
     */
    protected function createMongoSavepoint()
    {
    }

    /**
     * Handle an exception from a transaction beginning.
     * @param \MongoDB\Driver\Session $session
     * @param  \Throwable $e
     * @return void
     *
     * @throws \Exception
     * @throws Throwable
     */
    protected function handleBeginMongoTransactionException($e, \MongoDB\Driver\Session $session)
    {
        if ($this->causedByLostConnection($e)) {
            $this->reconnect();

            $session->startTransaction($session);
        } else {
            throw $e;
        }
    }

    /**
     * Commit the active database transaction.
     *
     * @param \MongoDB\Driver\Session $session
     * @return void
     */
    public function commitMongo(\MongoDB\Driver\Session $session)
    {
        if ($this->transactions == 1) {
            $session->commitTransaction();
        }

        $this->transactions = max(0, $this->transactions - 1);

        $this->fireConnectionEvent('committed');
    }

    /**
     * Rollback the active database transaction.
     *
     * @param \MongoDB\Driver\Session $session
     * @param  int|null $toLevel
     * @return void
     */
    public function rollBackMongo(\MongoDB\Driver\Session $session, $toLevel = null)
    {
        // We allow developers to rollback to a certain transaction level. We will verify
        // that this given transaction level is valid before attempting to rollback to
        // that level. If it's not we will just return out and not attempt anything.
        $toLevel = is_null($toLevel)
                    ? $this->transactions - 1
                    : $toLevel;

        if ($toLevel < 0 || $toLevel >= $this->transactions) {
            return;
        }

        // Next, we will actually perform this rollback within this database and fire the
        // rollback event. We will also set the current transaction level to the given
        // level that was passed into this method so it will be right from here out.
        $this->performMongoRollBack($session, $toLevel);

        $this->transactions = $toLevel;

        $this->fireConnectionEvent('rollingBack');
    }

    /**
     * Perform a rollback within the database.
     *
     * @param \MongoDB\Driver\Session $session
     * @param  int $toLevel
     * @return void
     */
    protected function performMongoRollBack(\MongoDB\Driver\Session $session, $toLevel)
    {
        if ($toLevel == 0) {
            $session->abortTransaction();
        }
    }

    /**
     * Get the number of active transactions.
     *
     * @return int
     */
    public function transactionLevel()
    {
        return $this->transactions;
    }
}
